﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sprechtag
{
    class Program
    {        
        public const string ConnectionStringUntis = @"Provider = Microsoft.Jet.OLEDB.4.0; Data Source=M:\\Data\\gpUntis.mdb;";        
        public static string DateiName = @"C:\Users\bm\Berufskolleg Borken\Kollegium - General\03 Schulleitung\3.04 Terminpläne\2020-21\12 Sprechtag\Sprechtag.xlsx";

        static void Main(string[] args)
        {
            Console.WriteLine("Terminplanung (Version 20201016)");
            Console.WriteLine("================================");
            Console.WriteLine("");

            int sj = (DateTime.Now.Month >= 8 ? DateTime.Now.Year : DateTime.Now.Year - 1);
            string aktSjUntis = sj.ToString() + (sj + 1);
            string aktSjAtlantis = sj.ToString() + "/" + (sj + 1 - 2000);

            Feriens feriens = new Feriens(aktSjUntis, ConnectionStringUntis);
            Periodes periodes = new Periodes(aktSjUntis, ConnectionStringUntis);
            Fachs fachs = new Fachs(aktSjUntis, ConnectionStringUntis);
            Unterrichtsgruppes unterrichtsgruppes = new Unterrichtsgruppes(aktSjUntis, ConnectionStringUntis);
            Raums raums = new Raums(aktSjUntis, ConnectionStringUntis, periodes);
            Lehrers lehrers = new Lehrers(aktSjUntis, raums, ConnectionStringUntis, periodes);
            Klasses klasses = new Klasses(aktSjUntis, lehrers, raums, ConnectionStringUntis, periodes);
            Unterrichts unterrichts = new Unterrichts(aktSjUntis, ConnectionStringUntis, periodes.AktuellePeriode, klasses, lehrers, fachs, raums, unterrichtsgruppes);

            unterrichts.Sprechtag(lehrers, raums, DateiName);
        }
    }
}
