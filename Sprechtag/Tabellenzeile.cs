﻿using System;
using System.Collections.Generic;

namespace Sprechtag
{
    public class Tabellenzeile
    {
        public int Zeile { get; private set; }
        public DateTime DatumUhrzeitVon { get; private set; }
        public DateTime DatumUhrzeitBis { get; private set; }
        public string Inhalt { get; private set; }
        public Lehrer Lehrer { get; private set; }
        public Raum Raum { get; private set; }

        public Tabellenzeile(int zeile, DateTime datumUhrzeitVon, DateTime datumUhrzeitBis, string inhalt, Raum raum, Lehrer lehrer )
        {
            Zeile = zeile;
            DatumUhrzeitVon = datumUhrzeitVon;
            DatumUhrzeitBis = datumUhrzeitBis;
            Inhalt = inhalt;
            Lehrer = lehrer;
            Raum = raum;
        }
    }
}