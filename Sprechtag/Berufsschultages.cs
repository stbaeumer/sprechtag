﻿using System.Collections.Generic;

namespace Sprechtag
{
    public class Berufsschultages : List<Berufsschultag>
    {
        private string aktSjUntis;
        private string connectionStringUntis;

        public Berufsschultages()
        {
        }

        public Berufsschultages(string aktSjUntis, string connectionStringUntis)
        {
            this.aktSjUntis = aktSjUntis;
            this.connectionStringUntis = connectionStringUntis;
        }
    }
}