﻿using Microsoft.Exchange.WebServices.Data;
using System;

namespace Sprechtag
{
    public class Lehrer
    {
        private string v1;
        private string v2;
        private int v3;

        public Lehrer()
        {
        }

        public Lehrer(string anrede, string nachname, string raum)
        {
            this.Anrede = anrede;
            this.Nachname = nachname;
            this.Raum = raum;
        }

        public int IdUntis { get; internal set; }
        public string Kürzel { get; internal set; }
        public string Mail { get; internal set; }
        public string Nachname { get; internal set; }
        public string Vorname { get; internal set; }
        public string Anrede { get; internal set; }
        public string Titel { get; internal set; }
        public string Raum { get; internal set; }
        public string Funktion { get; internal set; }
        public string Dienstgrad { get; internal set; }

        internal void ToExchange(
            DateTime datumMontagDerKalenderwoche,
            Feriens feriens,
            Unterrichts unterrichts,
            int periode,
            ExchangeService service,
            Periodes periodes,
            int kalenderwoche)
        {
            try
            {

                service.ImpersonatedUserId = new ImpersonatedUserId(
                    ConnectingIdType.SmtpAddress,
                    this.Mail);

                Appointments appointmentsIst = new Appointments(
                    this.Mail,
                    datumMontagDerKalenderwoche,
                    datumMontagDerKalenderwoche.AddDays(5),
                    service);

                Appointments appointmentsSoll = new Appointments(
                    "",
                    datumMontagDerKalenderwoche,
                    datumMontagDerKalenderwoche.AddDays(5),
                    service);

                appointmentsIst.DeleteAppointments(
                    appointmentsSoll);

                appointmentsSoll.AddAppointments(
                    appointmentsIst,
                    this,
                    service);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Fehler bei Lehrer " + this.Kürzel + "\n" + ex.ToString());
                throw new Exception("Fehler bei Lehrer " + this.Kürzel + "\n" + ex.ToString());
            }
        }
    }
}