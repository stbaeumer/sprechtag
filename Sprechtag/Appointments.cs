﻿using System;
using Microsoft.Exchange.WebServices.Data;
using System.Collections.Generic;
using System.Linq;

namespace Sprechtag
{
    public class Appointments : List<Appointment>
    {
        public Appointments(
            ExchangeService service)
        {
            try
            {

                /*
                foreach (var unterricht in (from s in kumulierteUnterrichteDiesesLehrers
                                            orderby s.Tag, s.Stunde, s.KlasseKürzel
                                            select s).ToList())
                {
                    Appointment appointment = new Appointment(service);
                    appointment.Categories.Add("Stundenplan");
                    appointment.Start = unterricht.Von;
                    appointment.End = unterricht.Bis;
                    appointment.IsAllDayEvent = false;
                    appointment.IsReminderSet = false;
                    appointment.Location = unterricht.Raum;
                    appointment.Subject = (unterricht.KlasseKürzel + " " + unterricht.FachKürzel + " " + unterricht.Raum);
                    appointment.Body = unterricht.Text;

                    this.Add(appointment);

                    Console.WriteLine("[Soll] " + appointment.Subject.PadRight(30) + appointment.Start + "-" + appointment.End.ToShortTimeString() + " " + appointment.Body.Text);
                }*/
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw new Exception(ex.ToString());
            }
        }

        public Appointments(
            string mail,
            DateTime beginn,
            DateTime ende,
            ExchangeService service)
        {
            try
            {
                CalendarView calView = new CalendarView(
                    beginn,
                    ende)
                {
                    PropertySet = new PropertySet(
                        BasePropertySet.IdOnly,
                        AppointmentSchema.Subject,
                        AppointmentSchema.Start,
                        AppointmentSchema.IsRecurring,
                        AppointmentSchema.AppointmentType,
                        AppointmentSchema.Categories)
                };

                FindItemsResults<Appointment> findResults = service.FindAppointments(
                    WellKnownFolderName.Calendar,
                    calView);

                List<Appointment> relevanteAppointments = new List<Appointment>();

                Console.WriteLine("Existierende Appointments für " + mail + ":");

                foreach (var appointment in findResults.Items)
                {
                    // Alle relevanten Appointments für diese Zielperson werden in eine Liste geladen, ...

                    appointment.Load();

                    // ... um die Eigenschaften von Appointment und Termin vergleichen zu können.

                    if (appointment.Categories.Contains("Stundenplan") || appointment.Categories.Contains("Ferien"))
                    {
                        // Wenn Subject oder Body null sind, werden sie durch "" ersetzt.

                        appointment.Subject = appointment.Subject ?? "";

                        appointment.Body = appointment.Body ?? "";

                        this.Add(appointment);
                        Console.WriteLine("[Ist ] " + appointment.Subject.PadRight(30) + appointment.Start + "-" + appointment.End.ToShortTimeString());
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Fehler beim Lehrer mit der Adresse " + mail + "\n" + ex.ToString());
                throw new Exception("Fehler beim Lehrer mit der Adresse " + mail + "\n" + ex.ToString());
            }
        }

        internal void DeleteAppointments(Appointments appointmentsSoll)
        {
            try
            {
                foreach (var appointmentIst in this)
                {
                    // Wenn das Ist-Appointment nicht in den Soll-Appointments existiert, ... 

                    if (!(from a in appointmentsSoll
                          where a.Subject == appointmentIst.Subject
                          where (a.Start == appointmentIst.Start && a.End == appointmentIst.End) || (a.Start.Date == appointmentIst.Start.Date && a.IsAllDayEvent == appointmentIst.IsAllDayEvent && appointmentIst.IsAllDayEvent == true)
                          where (appointmentIst.Categories.Contains("Stundenplan") || appointmentIst.Categories.Contains("Ferien"))
                          select a).Any())
                    {
                        // ... wird es gelöscht.

                        appointmentIst.Delete(
                            DeleteMode.HardDelete);

                        Console.WriteLine("[ -  ] " + appointmentIst.Subject.PadRight(30) + appointmentIst.Start + " - " + appointmentIst.End);
                    }
                }

                // Wenn ein Ist-Appointment aus irgendeinem Grund mehrfach angelegt wurde, ...

                var x = (from t in this orderby t.Start, t.End, t.Subject select t).ToList();

                for (int i = 1; i < x.Count(); i++)
                {
                    if (x[i].Subject == x[i - 1].Subject && x[i].Start == x[i - 1].Start && x[i].End == x[i - 1].End && (x[i].Categories.Contains("Stundenplan") && x[i - 1].Categories.Contains("Stundenplan") || x[i].Categories.Contains("Ferien") && x[i - 1].Categories.Contains("Ferien")))
                    {
                        // ... wird der Vorgänger gelöscht.

                        Console.WriteLine("[ -  ] " + x[i].Subject.PadRight(30) + x[i].Start + " - " + x[i].End + " gelöscht, da mehrfach existent.");

                        x[i - 1].Delete(
                            DeleteMode.HardDelete);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw new Exception(ex.ToString());
            }
        }

        internal void AddAppointments(
            Appointments appointmentsIst,
            Lehrer lehrer,
            ExchangeService service)
        {
            try
            {
                foreach (var appointmentSoll in this)
                {
                    // Wenn das Soll-Appointment nicht in den Ist-Appointments existiert, ... 

                    if (!(from a in appointmentsIst
                          where a.Subject == appointmentSoll.Subject
                          where a.Start == appointmentSoll.Start
                          where a.End == appointmentSoll.End
                          where (appointmentSoll.Categories.Contains("Stundenplan") || appointmentSoll.Categories.Contains("Ferien"))
                          select a).Any())
                    {
                        // ... wird angelegt

                        service.ImpersonatedUserId = new ImpersonatedUserId(
                            ConnectingIdType.SmtpAddress,
                            lehrer.Mail);

                        appointmentSoll.IsReminderSet = false;
                        appointmentSoll.Save();

                        Console.WriteLine("[ +  ] " + appointmentSoll.Subject.PadRight(30) + appointmentSoll.Start + " - " + appointmentSoll.End);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw new Exception(ex.ToString());
            }
        }
    }
}