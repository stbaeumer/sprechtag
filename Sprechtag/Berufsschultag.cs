﻿namespace Sprechtag
{
    public class Berufsschultag
    {
        public string Turnus { get; internal set; }
        public string Wochentag { get; internal set; }

        public Berufsschultag(string wochentag, string turnus)
        {
            if (wochentag == "1")
            {
                this.Wochentag = "montags";
            }
            if (wochentag == "2")
            {
                this.Wochentag = "dienstags";
            }
            if (wochentag == "3")
            {
                this.Wochentag = "mittwochs";
            }
            if (wochentag == "4")
            {
                this.Wochentag = "donnerstags";
            }
            if (wochentag == "5")
            {
                this.Wochentag = "freitags";
            }
            this.Turnus = turnus;
        }
    }
}