﻿using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;

namespace Sprechtag
{
    public class Lehrers : List<Lehrer>
    {
        public Lehrers()
        {
        }

        public Lehrers(string aktSj, Raums raums, string connectionString, Periodes periodes)
        {
            using (OleDbConnection oleDbConnection = new OleDbConnection(connectionString))
            {
                try
                {
                    string queryString = @"SELECT DISTINCT 
Teacher.Teacher_ID, 
Teacher.Name, 
Teacher.Longname, 
Teacher.FirstName,
Teacher.Email,
Teacher.Flags,
Teacher.Title,
Teacher.ROOM_ID,
Teacher.Text2,
Teacher.Text3,
Teacher.PlannedWeek
FROM Teacher 
WHERE (((SCHOOLYEAR_ID)= " + aktSj + ") AND  ((TERM_ID)=" + periodes.Count + ") AND ((Teacher.SCHOOL_ID)=177659) AND (((Teacher.Deleted)=No))) ORDER BY Teacher.Name;";

                    OleDbCommand oleDbCommand = new OleDbCommand(queryString, oleDbConnection);
                    oleDbConnection.Open();
                    OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader();

                    Lehrers unsortiert = new Lehrers();

                    while (oleDbDataReader.Read())
                    {
                        Lehrer lehrer = new Lehrer();

                        lehrer.Anrede = Global.SafeGetString(oleDbDataReader, 5) == "n" ? "Herr" : Global.SafeGetString(oleDbDataReader, 5) == "W" ? "Frau" : "";
                            lehrer.Titel = Global.SafeGetString(oleDbDataReader, 6);
                        lehrer.IdUntis = oleDbDataReader.GetInt32(0);
                        lehrer.Kürzel = Global.SafeGetString(oleDbDataReader, 1);
                        lehrer.Nachname = Global.SafeGetString(oleDbDataReader, 2) + (lehrer.Titel != "" ? ", " + lehrer.Titel : "");
                        lehrer.Vorname = Global.SafeGetString(oleDbDataReader, 3);
                        lehrer.Mail = Global.SafeGetString(oleDbDataReader, 4);
                        
                        lehrer.Raum = (from r in raums where r.IdUntis == oleDbDataReader.GetInt32(7) select r.Raumnummer).FirstOrDefault();
                        lehrer.Funktion = Global.SafeGetString(oleDbDataReader, 8);
                        lehrer.Dienstgrad = Global.SafeGetString(oleDbDataReader, 9);
                                                
                        if (lehrer.Kürzel != "?" && !lehrer.Mail.EndsWith("@berufskolleg-borken.de") && lehrer.Kürzel != "LAT")
                        {
                            if (lehrer.Kürzel != "?")
                            {
                                Console.WriteLine("Der Lehrer " + lehrer.Kürzel + " hat keine Mail-Adresse in Untis. Bitte in Untis eintragen. ENTER"); Console.ReadKey();
                            }
                            if (lehrer.Anrede == "")
                            {
                                Console.WriteLine("Der Lehrer " + lehrer.Kürzel + " hat keinGeschlecht in Untis. Bitte in Untis eintragen. ENTER"); Console.ReadKey();
                            }
                        }
                        
                        if(lehrer.Kürzel != "LAT")
                            unsortiert.Add(lehrer);
                    };

                    unsortiert.Add(new Lehrer("Frau", "Wenz, Dr., LWK NRW", "3301"));
                    unsortiert.Add(new Lehrer("Frau", "Kessens, LWK NRW", "3307"));

                    this.AddRange(unsortiert.OrderBy(y => y.Nachname).ThenBy(z => z.Anrede));

                    Console.WriteLine(("Lehrer " + ".".PadRight(this.Count / 150, '.')).PadRight(48, '.') + (" " + this.Count).ToString().PadLeft(4), '.');

                    oleDbDataReader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    throw new Exception(ex.ToString());
                }
                finally
                {
                    oleDbConnection.Close();
                }
            }
        }

        public void ToExchange(
            List<string> lehrerkürzels,
            DateTime datumMontagDerKalenderwoche,
            int periode,
            Periodes periodes,
            int kalenderwoche,
            bool interactiveMode)
        {
            ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2013)
            {
                UseDefaultCredentials = true
            };

            service.TraceEnabled = false;
            service.TraceFlags = TraceFlags.All;
            service.Url = new Uri("https://ex01.bkb.local/EWS/Exchange.asmx");

            foreach (var lehrer in (from l in this
                                    where lehrerkürzels.Contains(l.Kürzel)
                                    where l.Mail.ToLower().EndsWith("@berufskolleg-borken.de")
                                    where l.Kürzel != "?"
                                    select l
                                    ))
            {
                /*lehrer.ToExchange(
                    datumMontagDerKalenderwoche,
                    service,
                    periodes,
                    kalenderwoche);`*/


            }
        }
    }
}