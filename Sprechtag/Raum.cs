﻿namespace Sprechtag
{
    public class Raum
    {
        public int IdUntis { get; internal set; }
        public string Raumnummer { get; internal set; }
        public string Raumname { get; internal set; }
    }
}