﻿using System;
using System.Collections.Generic;

namespace Sprechtag
{
    public class Interruption
    {
        public List<DateTime> von = new List<DateTime>();
        public List<DateTime> bis = new List<DateTime>();
    }
}