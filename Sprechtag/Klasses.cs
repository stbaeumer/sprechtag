﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.IO;
using System.Linq;

namespace Sprechtag
{
    public class Klasses : List<Klasse>
    {
        public Lehrers Lehrers { get; set; }

        public Klasses(string aktSj, Lehrers lehrers, Raums raums, string connectionString, Periodes periodes)
        {
            Lehrers = lehrers;

            using (OleDbConnection oleDbConnection = new OleDbConnection(connectionString))
            {
                try
                {
                    string queryString = @"SELECT 
Class.CLASS_ID, 
Class.Name, 
Class.TeacherIds, 
Class.Longname, 
Teacher.Name,
Class.ClassLevel, 
Class.PERIODS_TABLE_ID,
Department.Name,
Class.TimeRequest,
Class.ROOM_ID,
Class.Text
FROM (Class LEFT JOIN Department ON Class.DEPARTMENT_ID = Department.DEPARTMENT_ID) LEFT JOIN Teacher ON Class.TEACHER_ID = Teacher.TEACHER_ID
WHERE (((Class.SCHOOL_ID)=177659) AND ((Class.TERM_ID)=" + periodes.Count + ") AND ((Class.Deleted)=False) AND ((Class.TERM_ID)=" + periodes.Count + ") AND ((Class.SCHOOLYEAR_ID)=" + aktSj + ") AND ((Department.SCHOOL_ID)=177659) AND ((Department.SCHOOLYEAR_ID)=" + aktSj + ") AND ((Teacher.SCHOOL_ID)=177659) AND ((Teacher.SCHOOLYEAR_ID)=" + aktSj + ") AND ((Teacher.TERM_ID)=" + periodes.Count + "))ORDER BY Class.Name ASC; ";

                    OleDbCommand oleDbCommand = new OleDbCommand(queryString, oleDbConnection);
                    oleDbConnection.Open();
                    OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader();

                    while (oleDbDataReader.Read())
                    {
                        List<Lehrer> klassenleitungen = new List<Lehrer>();

                        foreach (var item in (Global.SafeGetString(oleDbDataReader, 2)).Split(','))
                        {
                            klassenleitungen.Add((from l in lehrers
                                                  where l.IdUntis.ToString() == item
                                                  select l).FirstOrDefault());
                        }

                        var klasseName = Global.SafeGetString(oleDbDataReader, 1);


                        Klasse klasse = new Klasse()
                        {
                            IdUntis = oleDbDataReader.GetInt32(0),
                            NameUntis = klasseName,
                            Klassenleitungen = klassenleitungen,
                            Jahrgang = Global.SafeGetString(oleDbDataReader, 5),
                            Bereichsleitung = Global.SafeGetString(oleDbDataReader, 7),
                            Beschreibung = Global.SafeGetString(oleDbDataReader, 3),
                            Berufsschultage = GetBerufsschultage(Global.SafeGetString(oleDbDataReader, 5), Global.SafeGetString(oleDbDataReader, 8)),
                            Raum = (from r in raums where r.IdUntis == oleDbDataReader.GetInt32(9) select r).FirstOrDefault(),
                            Url = "https://www.berufskolleg-borken.de/bildungsgange/" + Global.SafeGetString(oleDbDataReader, 10)
                        };

                        this.Add(klasse);
                    };

                    Console.WriteLine(("Klassen " + ".".PadRight(this.Count / 150, '.')).PadRight(48, '.') + (" " + this.Count).ToString().PadLeft(4), '.');

                    oleDbDataReader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    throw new Exception(ex.ToString());
                }
                finally
                {
                    oleDbConnection.Close();
                }
            }
        }
        
        private Berufsschultages GetBerufsschultage(string classlevel, string timerequest)
        {
            var berufsschultages = new Berufsschultages();

            // Wenn es sich um eine Berufsschulklasse handelt, die nicht im 4. Jahrgang ist

            if (classlevel.Split('-')[0] == "BS" && classlevel.Split('-')[2] != "04")
            {
                foreach (var item in timerequest.Split(','))
                {
                    // Wenn in der ersten Stunde 

                    if (item.Length > 0 && item.Split('~')[1] == "1")
                    {
                        // ... ein Zeitwunsch von +1, +2 oder +3 steht.

                        if ((new List<string>() { "4", "5", "6" }).Contains(item.Split('~')[0]))
                        {
                            var wochentag = item.Split('~')[3];
                            var turnus = item.Split('~')[0] == "4" ? "ungerade KW" : item.Split('~')[0] == "5" ? "gerade KW" : "wöchentlich";
                            berufsschultages.Add(new Berufsschultag(wochentag, turnus));
                        }
                    }
                }
            }
            return berufsschultages;
        }

        internal Lehrers GetBereichleiter(List<string> beteiligteKlassen)
        {
            var beteiligteLehrers = new Lehrers();

            foreach (var klasse in this)
            {
                foreach (var beteiligteKlasse in beteiligteKlassen)
                {
                    if (klasse.NameUntis.StartsWith(beteiligteKlasse))
                    {
                        beteiligteLehrers.Add((from l in Lehrers where klasse.Bereichsleitung == l.Kürzel select l).FirstOrDefault());
                    }
                }
            }
            return beteiligteLehrers;
        }

        internal Lehrers GetKlassenleiter(List<string> beteiligteKlassen)
        {
            var beteiligteLehrers = new Lehrers();

            foreach (var klasse in this)
            {
                foreach (var beteiligteKlasse in beteiligteKlassen)
                {

                    if (klasse.NameUntis.StartsWith(beteiligteKlasse))
                    {
                        foreach (var klassenleitung in klasse.Klassenleitungen)
                        {
                            beteiligteLehrers.Add((from l in Lehrers where klassenleitung.Kürzel == l.Kürzel select l).FirstOrDefault());
                        }
                    }
                }
            }
            return beteiligteLehrers;
        }
    }
}